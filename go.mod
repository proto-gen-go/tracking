module gitlab.com/proto-gen-go/tracking

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	google.golang.org/genproto v0.0.0-20200702021140-07506425bd67
	google.golang.org/protobuf v1.25.0
)
